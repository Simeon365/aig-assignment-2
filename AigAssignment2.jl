### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ f61863c2-d104-11eb-127a-15b3a09b26cf
using Pkg

# ╔═╡ e728e230-d105-11eb-1f16-6b73b5a69184
using BSON

# ╔═╡ 3d995500-d106-11eb-3bd8-273dde5529e3
using Flux

# ╔═╡ f776ead0-d1df-11eb-1c0e-815d03f16330
using Flux.Data: DataLoader

# ╔═╡ fdcee900-d1df-11eb-331b-0fefb883fd56
using Flux.Optimise: Optimiser, WeightDecay

# ╔═╡ fd82ebe0-d1df-11eb-20a5-f3551dbbdc5c
using Flux: onehotbatch, onecold

# ╔═╡ fd644050-d1df-11eb-2a2c-db3e2b90747f
using Flux.Losses: logitcrossentropy

# ╔═╡ 8b672b72-d108-11eb-31d6-7b0949078dda
using CUDA

# ╔═╡ 1ea7c09e-d1dd-11eb-202b-13f571502967
using Statistics

# ╔═╡ 252f3342-d1dd-11eb-1698-538d80c36fb5
using Logging: with_logger

# ╔═╡ 2b802c90-d1dd-11eb-1010-017e7f2386cf
using TensorBoardLogger: TBLogger, tb_overwrite, set_step!, set_step_increment!

# ╔═╡ 55fca390-d1dd-11eb-0662-53870473d916
using ProgressMeter: @showprogress

# ╔═╡ 82ed17f0-d105-11eb-1631-6971dda43272
Pkg.activate("Project.toml")

# ╔═╡ 4299b270-d106-11eb-2bea-d3a05151c7a6
function LeNet5_Architecture(; imgsize=(28,28,1), nclasses=10) 
    out_conv_size = (imgsize[1]÷4 - 3, imgsize[2]÷4 - 3, 16)
    
    return Chain(
            Conv((5, 5), imgsize[end]=>6, relu),
            MaxPool((2, 2)),
            Conv((5, 5), 6=>16, relu),
            MaxPool((2, 2)),
            flatten,
            Dense(prod(out_conv_size), 120, relu), 
            Dense(120, 84, relu), 
            Dense(84, nclasses)
          )
end 

# ╔═╡ 813ba520-d10a-11eb-23f6-351742ddfab5
function load_data(args)
	train_path = "chest_xray/train/"
	test_path =  "chest_xray/test/"
	xtrain, ytrain = readdir(train_path)
	xtest, ytest = readdir(test_path)
	
	

    xtrain = reshape(xtrain, 28, 28, 1, :)
    xtest = reshape(xtest, 28, 28, 1, :)

    ytrain, ytest = onehotbatch(ytrain, 0:9), onehotbatch(ytest, 0:9)

    train_loader = DataLoader((xtrain, ytrain), batchsize=args.batchsize, shuffle=true)
    test_loader = DataLoader((xtest, ytest),  batchsize=args.batchsize)
    
    return train_loader, test_loader
end

# ╔═╡ 127c13d0-d1fb-11eb-0a1f-f735ea3a5bca
global labels =["Normal","Pneumonia"]

# ╔═╡ f3b4c0b0-d145-11eb-3566-317e1d4042a8
loss(ŷ, y) = logitcrossentropy(ŷ, y)

# ╔═╡ 044c306e-d146-11eb-13e7-a19f93118374
num_parameters(model) = sum(length, Flux.params(model)) 

# ╔═╡ 0d3b0622-d146-11eb-0759-cbbc3ccd2ab7
round4(x) = round(x, digits=4)

# ╔═╡ f6d2f0a2-d145-11eb-369f-13a307beaa22
function evaluate_loss_accuracy(loader, model, device)
    l = 0f0
    acc = 0
    ntot = 0
    for (x, y) in loader
        x, y = x |> device, y |> device
        ŷ = model(x)
        l += loss(ŷ, y) * size(x)[end]        
        acc += sum(onecold(ŷ |> cpu) .== onecold(y |> cpu))
        ntot += size(x)[end]
    end
    return (loss = l/ntot |> round4, acc = acc/ntot*100 |> round4)
end

# ╔═╡ 15fa6760-d146-11eb-190a-8f5739d2dc6f
Base.@kwdef mutable struct Args
    η = 3e-4             
    λ = 0               
    batchsize = 128     
    epochs = 10          
    seed = 0             
    use_cuda = true      
    infotime = 1 	     
    checktime = 5        
    tblogger = true     
    savepath = "Assignment2/"
end

# ╔═╡ 32987420-d146-11eb-0007-6de336217261
function Train_Model(; kws...)
    args = Args(; kws...)
    args.seed > 0 && Random.seed!(args.seed)
    use_cuda = args.use_cuda && CUDA.functional()
    
    if use_cuda
        device = gpu
        @info "Training on GPU"
    else
        device = cpu
        @info "Training on CPU"
    end

    ## DATA
    train_loader, test_loader = load_data(args)

    ## MODEL AND OPTIMIZER
    model = LeNet5() |> device    
    
    ps = Flux.params(model)  

    opt = ADAM(args.η) 
    if args.λ > 0
        opt = Optimiser(WeightDecay(args.λ), opt)
    end
    
    ## LOGGING UTILITIES
    if args.tblogger 
        tblogger = TBLogger(args.savepath, tb_overwrite)
        set_step_increment!(tblogger, 0) 
        @info "TensorBoard logging at \"$(args.savepath)\""
    end
    
    function report(epoch)
        train = eval_loss_accuracy(train_loader, model, device)
        test = eval_loss_accuracy(test_loader, model, device)        
        println("Epoch: $epoch   Train: $(train)   Test: $(test)")
        if args.tblogger
            set_step!(tblogger, epoch)
            with_logger(tblogger) do
                @info "train" loss=train.loss  acc=train.acc
                @info "test"  loss=test.loss   acc=test.acc
            end
        end
    end
    
    ## TRAINING
    report(0)
    for epoch in 1:args.epochs
        @showprogress for (x, y) in train_loader
            x, y = x |> device, y |> device
            gs = Flux.gradient(ps) do
                    ŷ = model(x)
                    loss(ŷ, y)
                end

            Flux.Optimise.update!(opt, ps, gs)
        end
        
        ## Printing and logging
        epoch % args.infotime == 0 && report(epoch)
        if args.checktime > 0 && epoch % args.checktime == 0
            !ispath(args.savepath) && mkpath(args.savepath)
            modelpath = joinpath(args.savepath, "model.bson") 
            let model = cpu(model) 
                BSON.@save modelpath model epoch
            end
            @info "Model saved in \"$(modelpath)\""
        end
    end
end

# ╔═╡ Cell order:
# ╠═f61863c2-d104-11eb-127a-15b3a09b26cf
# ╠═82ed17f0-d105-11eb-1631-6971dda43272
# ╠═e728e230-d105-11eb-1f16-6b73b5a69184
# ╠═3d995500-d106-11eb-3bd8-273dde5529e3
# ╠═f776ead0-d1df-11eb-1c0e-815d03f16330
# ╠═fdcee900-d1df-11eb-331b-0fefb883fd56
# ╠═fd82ebe0-d1df-11eb-20a5-f3551dbbdc5c
# ╠═fd644050-d1df-11eb-2a2c-db3e2b90747f
# ╠═8b672b72-d108-11eb-31d6-7b0949078dda
# ╠═1ea7c09e-d1dd-11eb-202b-13f571502967
# ╠═252f3342-d1dd-11eb-1698-538d80c36fb5
# ╠═2b802c90-d1dd-11eb-1010-017e7f2386cf
# ╠═55fca390-d1dd-11eb-0662-53870473d916
# ╠═4299b270-d106-11eb-2bea-d3a05151c7a6
# ╠═813ba520-d10a-11eb-23f6-351742ddfab5
# ╠═127c13d0-d1fb-11eb-0a1f-f735ea3a5bca
# ╠═f3b4c0b0-d145-11eb-3566-317e1d4042a8
# ╠═f6d2f0a2-d145-11eb-369f-13a307beaa22
# ╠═044c306e-d146-11eb-13e7-a19f93118374
# ╠═0d3b0622-d146-11eb-0759-cbbc3ccd2ab7
# ╠═15fa6760-d146-11eb-190a-8f5739d2dc6f
# ╠═32987420-d146-11eb-0007-6de336217261
